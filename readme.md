# Projet Master Mind

Implementation du master mind avec Python3 et Pygame

## Fonctionnalites :

Le joueur doit deviner une combinaison de couleur
Il perd si il depasse le nombre d'essais
Il gagne si il trouve la combinaison avant
Le jeu doit etre jouable a la souris

## Previsions

10 min de preparation papier
10 min de preparation PC

10 x 25 min pour faire un prototype jouable
10 x 25 min pour faire une version plus polish

## Idees :

Pouvoir choisir la taille de la combinaison et du nombre d'essais (ajouter les indication sur la combinatoire associee pour donner une idee a l'utilisateur de la difficulte)
Pouvoir enregistrer les Scores
Pouvoir jouer contre une IA en parrallele (le premier a trouver gagne)
pouvoir jouer contre un humain qui choisit la combinaison
pouvoir utiliser plus ou moins de couleur
Faire des modes accessibles aux daltoniens et/ou aveugles
Avoir des pions d'indication

## Suivis :

1 ere session

25 min, Set Up object : config, inputhandlers, state, mise en place premiere boucle
5 min, pause
25 min, Creation d'objet text pour gerer l'affichage du contenu, Fabrication du premier state (ecran introduction)
5 min, pause
25 min, Ajout de l'ensemble des State pour implementer le jeu de base, INTRO, CHOIX_JEU, JEU4X10, JEU5X12, VICTOIRE, DEFAITE

Fin session

2 eme session

25 min, Ajout objet MasterMind, debut implementation, ajout test unitaire
5 min, pause
25 min, Ajout input user en console et check de victoire a tester et refactoriser
5 min, pause
25 min, Ajout Classe Draw Master mind qui se chargera de l'affichage du jeu

Fin session

3 eme session

25 min, Debut dessin Grid, ligne et Secret
5 min pause,
25 min, Dessin plot, dessin selecteur ok, debut gestion souris
5 min pause,
25 min, dessin des plot dans les lignes
5 min pause,
25 min, Debut fonctionnement Ajout et Suppression sur la ligne courrante

Fin Session

### Observation
Le prototype est encore loin d'etre fonctionnel, jai de grosse difficultes entre le code d'affichage et la detection de position de la souris, je ne sais pas encore comment je vais m'en tirer. Je me rajoute une premiere fois 4 x 25 min, au terme de quoi j'aviserai sur la suite a donner

4 eme session

25 min, Debug et Avancement sur l'utilisation de rectangle pour gerer les collisions
5 min pause, 
25 min, debut refacto pour affichage du master mind

5 eme session

25 min, refacto stockage information de grid et tips dans le master mind
5 min pause,
25 min, refacto refacto refacto, surtout dans la classe de dessin du MM

Fin de session

## Observation

Le prototype n'est toujours pas jouable, il est donc en retard de 2h sur le budget temps. Le budget a ete deja une fois repousse, mais la refactorisation parait trop important pour terminer dans un delais raisonable. Le lien entre MM et sa classe de dessin est trop complique

Pour le moment je suspend le projet pour pouvoir passer a autre chose

## Observation

En fait j'ai debugger un gros soucis...

7 eme session

25 min, Ajout affichage du secret, debug de la condition d'echec, ajout gestion du state de Master mind dans l'objet state

Fin Session : Prototype Fonctionnel termine

## Observation

Afin de paufiner le prototype et malgre le tard que celui ci a pris, je decide de ne par rogner sur mon budget temps pour le paufinage et m'octroie donc 10 x 25 min pour le faire

## Paufinage :

1 ere session 

25 min, Fix bug sur les tips et quelques correction graphiques
5 min pause,
25 min, Refacto et Ajout ecran selection Humain vs IA
15 min pause,
25 min, debut implementation adversaire, correction graphiques
5 min pause,
25 min, Ajout selection entre humain et IA Fully functionnal!

Fin de session, Reste 6 x 25 min

25 min, debug, des tips => OK
5 min pause,
25 min, debut implementation tableau de score

Fin de session

25 min, Ajout entree du nom dans le tableau de score
5 min pause,
25 min, debut fix dernier bug connus, ajout consultation du tableau de score depuis le menu

Fin de session, Reste 2 x 25 min

25 min, Ajout texte Ecran principal du jeu, Debut ajout des sons
5 min, pause,
15 min, Ajout des sons selon les evenements du jeu

Fin de Projet
Observation, Projet difficile, Je dois trouver un moyen de structurer le code pour rendre plus facile l'integration des graphisme et de leur colisions
Ici le code est trop en spaghetti et ca part un peu dans tout les sens, c'est dommage