import pygame

from config import Config
from inputHandlers import InputHandlers
from state import State

def main():
    config = Config()
    screen = config.get_screen()

    clock = config.get_clock()

    input_handler = InputHandlers()
    state = State(config, input_handler)
    
    while True:

        status = state.get_state()
        if status == "INTRO":
            state.intro()
        
        if status == "CHOIX_ADV":
            state.choix_adv()

        if status == "CHOIX_JEU":
            state.choix_jeu()
        
        if status == "JEU4X10":
            state.jeu([4,10])
        
        if status == "JEU5X12":
            state.jeu([5,12])
        
        if status == "SCORE":
            state.highscore()

        if status == "VICTOIRE":
            state.victoire()
            state.highscore(False)
            state.highscore()
        
        if status == "DEFAITE":
            state.defaite()

        if status == "QUIT":
            pygame.quit()
            break


        pygame.display.update()

        screen.fill((0,0,0))
        clock.tick(30)

if __name__ == "__main__":

    pygame.init()

    main()