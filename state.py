import pygame
from config import Config
from inputHandlers import InputHandlers
from text import Text
from drawMasterMind import DrawMasterMind
from mastermind import MasterMind
from score import Score

class State:

    def __init__(self,config:Config,IH:InputHandlers) -> None:
        self.config = config
        self.IH = IH
        self.MM = None
        self.draw_MM = None
        self.state = "INTRO"

        self.game_adv = "IA"
        self.sound_flag = True

    def intro(self):

        result =self.IH.intro()
        if result == "QUIT":
            self.state = "QUIT"
        if result == "1":
           self.state = "CHOIX_ADV"
        if result == "2":
            self.state = "SCORE"
        if result == "3":
            self.state = "QUIT"

        text1 = Text("1. Nouvelle Partie",(128,128))
        text2 = Text("2. Tableau de Score",(128,192))
        text3 = Text("3. Quitter", (128, 256))
        text1.print_screen_text(self.config)
        text2.print_screen_text(self.config)
        text3.print_screen_text(self.config)
    
    def choix_adv(self):

        result = self.IH.get_key_down()
        if result == "QUIT":
            self.state = "QUIT"
        if result == "1":
            self.game_adv =  "IA"
            self.state = "CHOIX_JEU"
        if result == "2":
            self.game_adv = "HUMAN"
            self.state = "CHOIX_JEU"
        if result == "3":
            self.state = "INTRO"

        text1 = Text("1. Partie Contre IA/CPU", (128,128))
        text2 = Text("2. Partie Contre HUMAN (not Working)", (128, 192))
        text3 = Text("3. Retour Menu Principal", (128,256))
        text1.print_screen_text(self.config)
        text2.print_screen_text(self.config)
        text3.print_screen_text(self.config)
    
    def choix_jeu(self):

        result = self.IH.get_key_down()
        if result == "QUIT":
            self.state = "QUIT"
        if result == "1":
            self.state = "JEU4X10"
        if result == "2":
            self.state = "JEU5X12"
        if result == "3":
            self.state = "INTRO"

        text1 = Text("1. Partie 4 trous 10 essais",(128,128))
        text2 = Text("2. Partie 5 trous 10 essais", (128, 192))
        text3 = Text("3. Retour Menu Principal", (128, 256))
        text1.print_screen_text(self.config)
        text2.print_screen_text(self.config)
        text3.print_screen_text(self.config)

    def jeu(self, param):

        self.MM = MasterMind(self.config, param, self.game_adv)
        self.Draw_MM = DrawMasterMind(self.config, self.MM)

        while True:

            state_MM = self.MM.get_status()
            result = self.IH.get_key_down()
            if result == "QUIT":
                self.state = "QUIT"
                break
            if state_MM == "VICTORY":
                self.state = "VICTOIRE"
                break
            if state_MM == "FAIL":
                self.state = "DEFAITE"
                break
            if result == "P":
                print(self.MM)
            if result == "S":
                self.MM.flip_show_secret()

            self.Draw_MM.draw_master_mind()

            # print(pygame.mouse.get_pos())
            pygame.display.update()


    def highscore(self, read=True):
        name = ""
        while True:
            self.config.get_screen().fill((0,0,0))
            #Boucle pour entrez le nom du joueur
            result = self.IH.get_key_down()
            if result != None:
                if result == "ENTER":
                    if not read:
                        Score().add_score([name, "1"])
                        read = True
                    else:
                        self.state = "INTRO"
                    break
                if result == "BACKSPACE" and not read:
                    name = name[:-1]
                elif len(result) == 1 and result != " ":
                    if len(name) < 12:
                        name+=result
            
            text1 = Text("Tableau HIGHSCORE :",(128,128))
            text2 = Text("Appuyez sur Enter pour retourner au menu principal", (128, 608))
            scores = Score().get_score()
            i = 0
            text_score = []
            for score in scores:
                text = Text(str(i)+":"+score[0]+":"+score[1]+"Pts",(128,128+(32*i)))
                text_score.append(text)
                text.print_screen_text(self.config)
                i += 1
            if not read:
                text_name = Text("Entrez Votre Nom : "+name,(128,128+(32*i)))

            text1.print_screen_text(self.config)
            text2.print_screen_text(self.config)
            if not read:
                text_name.print_screen_text(self.config)
            pygame.display.update()


        

    def victoire(self):
        if self.sound_flag:
            self.config.play_sounds("victory")
            self.sound_flag = False
        result = self.IH.get_key_down()
        if result != None:
            self.state = "INTRO"
            self.sound_flag = True

        text1 = Text("!VICTOIRE!",(128,128))
        text2 = Text("appuyez sur une touche", (128, 192))
        text3 = Text("pour aller au tableau des HIGHSCORE", (128, 256))
        text1.print_screen_text(self.config)
        text2.print_screen_text(self.config)
        text3.print_screen_text(self.config)

    def defaite(self):
        if self.sound_flag:
            self.config.play_sounds("fail")
            self.sound_flag = False
        result = self.IH.get_key_down()
        if result != None:
            self.state = "INTRO"
            self.sound_flag = True

        text1 = Text("!DEFAITE!",(128,128))
        text2 = Text("appuyez sur une touche", (128, 192))
        text3 = Text("pour revenir au menu principal", (128, 256))
        text1.print_screen_text(self.config)
        text2.print_screen_text(self.config)
        text3.print_screen_text(self.config)


    def get_state(self):
        return self.state