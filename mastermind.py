import random
from config import Config

class MasterMind:

    def __init__(self, config:Config, param, adv= "IA") -> None:
        # param = [trou, essais]
        self.config = config
        self.trou = param[0]
        self.essais = param[1]

        self.show_secret = False
        if adv == "IA":
            self.combinaison = self.new_combinaison()
        else:
            self.combinaison = [None]*self.trou
            self.show_secret = True
        self.num_curr_line = 0
        self.current_select = 0
        self.lines = [[None for i in range(self.trou)] for i in range(self.essais)]
        self.tips = [[None for i in range(self.trou)] for i in range(self.essais)]
        self.status = "ALIVE"
        

        self.adv = adv
        
        self.color_code = ["RED","YELLOW","BLUE","ORANGE",
        "GREEN","WHITE","PURPLE","PINK"]
    
    def __str__(self):
        res = ""
        res += "Print Class MasterMind\n"
        res += "combinaison secrete :"+str(self.get_combinaison())+"\n"
        res += "liste de lignes et tips :\n"
        i = 0
        while i < self.num_curr_line:
            line = self.lines[i]
            tips = self.tips[i]
            res += str(line)+str(tips)+"\n"
            i += 1
            
        return res
    
    def get_status(self):
        return self.status
    
    def get_show_secret(self):
        return self.show_secret

    def get_color_code(self):
        return self.color_code
    
    def get_current_select(self):
        return self.current_select
    
    def set_current_select(self, value):
        if value >= 0 and value < 8:
            self.config.play_sounds("pickup")
            self.current_select = value
    
    def flip_show_secret(self):
        self.show_secret = False if self.show_secret else True 
    
    def scroll_current_select(self, scroll):
        self.current_select += scroll
        self.current_select = self.current_select%8
    
    def add_to_current(self, value, pos):
        # Adding to the current line but with a pos argument
        if pos < self.trou:
            self.config.play_sounds("put")
            self.lines[self.num_curr_line][pos] = value
    
    def remove_to_current(self, pos):
        if pos < self.trou:
            self.lines[self.num_curr_line][pos] = None
    
    def add_to_combinaison(self, value, pos):
        if pos < self.trou:
            self.config.play_sounds("put")
            self.combinaison[pos] = value
    
    def remove_to_combinaison(self, pos):
        if pos < self.trou:
            self.combinaison[pos] = None
    
    def validate_combinaison(self):
        if not None in self.combinaison:
            self.config.play_sounds("pickup")
            self.adv = "IA"
            self.show_secret = False

    def new_combinaison(self):
        res = []
        i = 0
        while i < self.trou:
            res.append(random.randrange(8))
            i += 1
        return res
    
    def set_combinaison(self):
        res = []
        i = 0
        while i < self.trou:
            res.append(i)
            i += 1
        self.combinaison = res

    def get_combinaison(self):
        return self.combinaison
    
    def set_line(self, line, pos):
        # Fonction pour test
        if pos < self.essais:
            self.lines[pos] = line
    
    def validate_line(self):
        # L'utilisateur appuis sur la validation de la ligne
        if None not in self.lines[self.num_curr_line]:
            self.config.play_sounds("pickup")
            self.num_curr_line += 1
            self.check_last_line()
            # if self.num_curr_line >= self.essais:
            #     self.status == "FAIL"
    
    def check_last_line(self):
        res = self.tips[self.num_curr_line-1]
        copy_combi = self.combinaison[:]
        if self.num_curr_line>0: # Peut etre pas necessaire
            line = self.lines[self.num_curr_line-1]

            color_already_passed = []
            #Premiere passe pour les position
            i = 0
            while i < self.trou:
                if line[i] == copy_combi[i]:
                    color_already_passed.append(line[i])
                    self.adding_tips_info(res, "POS")
                    copy_combi[i] = None
                i += 1
            #Seconde passe pour les info de couleur
            i = 0
            while i < self.trou:
                if line[i] in copy_combi:
                    j = 0
                    while j < self.trou:
                            if line[i] == copy_combi[j]:
                                copy_combi[j] = None
                            j+= 1
                    if line[i] not in color_already_passed:
                        color_already_passed.append(line[i])
                        self.adding_tips_info(res, "COLOR")
                i += 1
            
            # self.tips[i] = res
            self.check_victory()

        if self.num_curr_line == self.essais and self.status != "VICTORY":
            self.status =  "FAIL"
            self.show_secret = True
    
    def adding_tips_info(self, res, info):
        # Fonction qui remplace None le plus proche par info
        i = 0
        for elem in res:
            if elem == None:
                res[i] = info
                return None
            i += 1

    def check_victory(self):

        victory = True

        for tip in self.tips[self.num_curr_line-1]:
            if tip == "COLOR" or tip == None:
                victory = False
        
        if victory:
            self.status = "VICTORY"
            self.show_secret = True
    
    def input_console(self):
        # Fonction pour pouvoir prototyper en console
        res = []
        i = 0
        while i < self.trou:
            user_input = input("Entrez Une couleur : ")
            res.append(int(user_input))
            i += 1
        self.lines[self.num_curr_line] = res
        self.validate_line()

        print(self)
    

if __name__ == "__main__":

    def test_fonctionnel_2():
        config = Config()

        MM = MasterMind(config, [4,10])

        while MM.get_status()=="ALIVE":
            print(MM.get_status())
            MM.input_console()
        print(MM)
        print(MM.get_status())

    def test_fonctionnel_1():
        config = Config()

        line_test1 = [0,1,0,1] # POS POS
        line_test2 = [0,0,0,0] # COLOR
        line_test3 = [1,2,3,4] # COLOR COLOR COLOR
        line_test4 = [0,2,3,0] # POS COLOR COLOR
        line_test5 = [0,1,2,3] # POS POS POS POS => VICTORY

        MM = MasterMind(config, [4,10])
        MM.set_combinaison()

        MM.set_line(line_test1)
        MM.set_line(line_test2)
        MM.set_line(line_test3)
        MM.set_line(line_test4)
        MM.set_line(line_test5)

        print(MM)
    
    test_fonctionnel_2()