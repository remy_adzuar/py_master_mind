import pygame

class Config:

    def __init__(self) -> None:
        self.screen = pygame.display.set_mode((1280,720))
        self.clock = pygame.time.Clock()
        self.offset = [40,10]

        self.sounds = {}
        self.add_sound("pickup","./sounds/pickup.wav")
        self.add_sound("put","./sounds/put.wav")
        self.add_sound("fail","./sounds/fail.wav")
        self.add_sound("victory","./sounds/victory.wav")
    
    def get_screen(self):
        return self.screen
    
    def get_clock(self):
        return self.clock
    
    def get_offset(self):
        return self.offset
    
    def add_sound(self,name:str, URL:str):
        self.sounds[name] = pygame.mixer.Sound(URL)

    def get_sound(self, name:str):
        return self.sounds[name]

    def play_sounds(self, name:str):
        pygame.mixer.Sound.play(self.get_sound(name))
    