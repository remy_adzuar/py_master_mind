import pygame

from config import Config
from mastermind import MasterMind
from inputHandlers import ClickHandler
from text import Text

class DrawMasterMind:

    def __init__(self, config:Config, MM:MasterMind) -> None:
        self.config = config
        self.MM = MM
        self.click_handler = ClickHandler()
        self.BLACK = (0,0,0)
        self.GREY = (100,100,100)

        self.RED = (255,0,0)
        self.GREEN = (0,255,0)
        self.BLUE = (0,0,255)
        self.WHITE = (255,255,255)
        self.ORANGE = (255,128,0)
        self.PURPLE = (127,0,255)
        self.YELLOW = (255,255,0)
        self.PINK = (255,192,203)

        self.color_name = [self.RED, self.YELLOW, self.BLUE, self.ORANGE,
        self.GREEN, self.WHITE, self.PURPLE, self.PINK]

        self.last_click_grid_pos = None

        # Objet qui regroupe les position TopLeft(x,y)
        self.positions = {
            "grid": [24,0,280,700],
            "selector": [790,530,6*53,3*53],
            "validator": [790,430,1*53,1*53],
            "secret": [26,(53*self.MM.essais)+2,276,53]
        }

        self.grid = list()
        self.set_grid()
        self.validator = self.offset_coords(self.positions["validator"])
        self.selector = list()
        self.set_selector()
        self.combinaison = list()
        self.set_combinaison()

    def offset_coords(self, coords):
        offset = self.config.get_offset()
        if len(coords) == 4:
            return (pygame.Rect(coords[0]+offset[0], coords[1]+offset[1], coords[2], coords[3]))
        if len(coords) == 2:
            return (coords[0]+offset[0], coords[1]+offset[1])
    
    def set_grid(self):
        offset = self.config.get_offset()
        coords = self.positions["grid"]
        i = 0
        while i < self.MM.essais:
            self.grid.append(list())
            j = 0
            while j < self.MM.trou:
                rect = self.offset_coords(pygame.Rect(
                    coords[0]+2+(j*53),
                    coords[1]+2+(i*53),
                    53,
                    48
                ))
                self.grid[i].append(rect)
                j += 1
            i += 1
        
    def set_selector(self):
        coords = self.positions["selector"]
        i = 0
        while i < len(self.MM.color_code):
            j = 1 # Pour gerer le positionnement des cercles
            if i < 4:
                j = 0
            rect = self.offset_coords(pygame.Rect(
                coords[0]+(i%4)*53,
                coords[1]+(j*53),
                53,
                53
            ))
            self.selector.append(rect)
            i+=1
    
    def set_combinaison(self):
        coords = self.positions["secret"]
        i = 0
        while i < self.MM.trou:
            rect = self.offset_coords(pygame.Rect(
                coords[0]+(i*53),
                coords[1],
                53,
                53
            ))
            self.combinaison.append(rect)
            i += 1
    
    def draw_grid(self):
        screen = self.config.get_screen()
        grid_coords = self.offset_coords(self.positions["grid"])
        pygame.draw.rect(screen, self.WHITE, grid_coords)
        i = 0
        for line in self.grid:
            pos = line[0].topleft
            coords = ((pos[0], pos[1]),(276,51))
            pygame.draw.rect(screen, self.BLACK, coords)
            j = 0
            for case in line:
                pygame.draw.circle(screen, self.GREY, case.center, 24)

                j += 1

            if i < self.MM.essais:
                self.draw_line(self.MM.lines[i], i)    
            i += 1
    
    def draw_validator(self):
        screen = self.config.get_screen()
        grid_coords = self.validator
        text_pos = (grid_coords[0]+24,grid_coords[1]-16)
        pygame.draw.rect(screen, self.BLACK, grid_coords)
        pygame.draw.circle(screen, self.GREEN, grid_coords.center, 20)
        Text("Valider", text_pos, True).print_screen_text(self.config)

    def draw_master_mind(self):
        self.draw_bg()
        self.draw_selector()
        self.draw_validator()
        self.draw_selected_color()
        self.draw_grid()
        self.draw_secret()

        self.draw_tips()

        # Gestion des clicks
        if self.MM.adv == "HUMAN":
            self.Human_combi_select()

        if self.MM.adv == "IA":
            self.resolution_master_mind()
        
        # self.check_grid_collision()
    
    def Human_combi_select(self):
        cursor = self.click_handler.get_left_click()
        if cursor:
            collide_pos = self.check_combinaison_collision()
            if collide_pos != None:
                self.MM.add_to_combinaison(self.MM.current_select, collide_pos)
            self.check_selector_collision()
            self.check_validator_collision()
        
        cursor = self.click_handler.get_right_click()
        if cursor:
            collide_pos = self.check_combinaison_collision()
            if collide_pos != None:
                self.MM.remove_to_combinaison(collide_pos)

    
    def resolution_master_mind(self):
        cursor_pos = False
        cursor_pos = self.click_handler.get_left_click()
        if cursor_pos:
            collide_pos = self.check_grid_collision()
            if collide_pos != None:
                if collide_pos[0] == self.MM.num_curr_line:
                    self.MM.add_to_current(self.MM.current_select, collide_pos[1])
            self.check_validator_collision()
            self.check_selector_collision()
        remove = False
        remove = self.click_handler.get_right_click()
        if remove:
            collide_pos = self.check_grid_collision()
            if collide_pos != None:
                if collide_pos[0] == self.MM.num_curr_line:
                    self.MM.remove_to_current(collide_pos[1])

    def draw_bg(self):
        screen = self.config.get_screen()
        pygame.draw.rect(screen,self.GREY, self.offset_coords((0,0,1200,700)))
    
    def draw_selector(self):
        screen = self.config.get_screen()

        coords = self.offset_coords(self.positions["selector"])
        pygame.draw.rect(screen, self.BLACK, coords)
        i = 0
        for case in self.selector:
            pygame.draw.circle(screen,self.MM.color_code[i], case.center,24)
            i+= 1

    
    def draw_selected_color(self):
        screen = self.config.get_screen()
        coords = self.offset_coords((730,530,53,53))

        text_pos = (coords[0]+20,coords[1]-16)

        pygame.draw.rect(screen, self.BLACK,coords)
        color = self.color_name[self.MM.current_select]
        pygame.draw.circle(screen, color, coords.center, 22)
        Text("Color",text_pos,True).print_screen_text(self.config)
        

    def draw_secret(self):
        screen = self.config.get_screen()
        coords = self.offset_coords(self.positions["secret"])
        pygame.draw.rect(screen, self.BLACK, coords)
        line = self.MM.combinaison
        if self.MM.get_show_secret():
            i =  0
            for plot in line:
                coords_plot = (coords[0]+24+53*i,coords[1]+24)
                color = self.GREY
                if plot != None:
                    color = self.color_name[plot]
                self.draw_plot(coords_plot, color, 24)
                i += 1
        else:
            text_pos = (coords[0]+32,coords[1]+12)
            Text("Combinaison", text_pos).print_screen_text(self.config)
        # pygame.draw.rect(screen, self.GREY, self.offset_coords((26,2+nb*53, 261, 48)))

    def draw_line(self, line, pos):
        i = 0
        for plot in line:
            coords = self.offset_coords((28+24+i*53,pos*53+26))
            if plot != None:
                self.draw_plot(coords,self.color_name[plot])
            else:
                self.draw_plot(coords, self.GREY)
            i += 1

    def draw_plot(self, pos, color, r = 24):
        screen = self.config.get_screen()
        pygame.draw.circle(screen, color, pos, r)


    def draw_tips(self):
        screen = self.config.get_screen()
        i = 0
        x_coord = 320
        length = 53*self.MM.essais
        pygame.draw.rect(screen, self.BLACK, self.offset_coords(
            (x_coord,2,40+(20*self.MM.trou),length)
        ))
        for line in self.MM.tips:
            self.draw_one_tips(i, x_coord)
            i += 1
    
    def draw_one_tips(self, pos, x_coords):
        screen = self.config.get_screen()
        line = self.MM.tips[pos]
        i = 0
        for tip in line:
            color = self.GREY
            if tip == "COLOR":
                color = self.WHITE
            elif tip == "POS":
                color = self.RED
            if pos > self.MM.num_curr_line:
                color = self.BLACK
            pygame.draw.circle(screen, color, 
            self.offset_coords((x_coords+20+(i*24),24+(53*pos))),10)
            i += 1

    def check_grid_collision(self):
        i = 0
        mouse_pos = pygame.mouse.get_pos()
        while i < len(self.grid):
            j = 0
            line = self.grid[i]
            while j < len(line):
                case = line[j]
                if case.collidepoint(mouse_pos):
                    return [i,j]
                j += 1
            i += 1
        return None
    
    def check_selector_collision(self):
        i = 0
        mouse_pos = pygame.mouse.get_pos()
        while i < len(self.selector):
            j = 1
            if i < 4:
                j = 0
            case = self.selector[i]
            if case.collidepoint(mouse_pos):
                self.MM.set_current_select(i)
            i += 1
    
    def check_combinaison_collision(self):
        i = 0
        mouse_pos = pygame.mouse.get_pos()
        while i < len(self.combinaison):
            case = self.combinaison[i]
            if case.collidepoint(mouse_pos):
                return i
            i += 1
    
    def check_validator_collision(self):
        mouse_pos = pygame.mouse.get_pos()
        if self.validator.collidepoint(mouse_pos):
            if self.MM.adv == "IA":
                self.MM.validate_line()
            if self.MM.adv == "HUMAN":
                self.MM.validate_combinaison()
