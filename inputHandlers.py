import pygame

class InputHandlers():

    def __init__(self) -> None:
        pass

    def intro(self):

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return "QUIT"
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_1:
                    return "1"
                if event.key == pygame.K_2:
                    return "2"
                if event.key == pygame.K_3:
                    return "3"
    
    def get_key_down(self):

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return "QUIT"
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    return "ENTER"
                if event.key == pygame.K_BACKSPACE:
                    return "BACKSPACE"
                
                key = event.unicode
                return key.upper()
                

class ClickHandler():

    def __init__(self) -> None:
        self.left_state = False
        self.right_state = False

    def get_left_click(self):
        state = pygame.mouse.get_pressed()[0]
        if state and not self.left_state:
            self.left_state = True
            return True
        if not state and self.left_state:
            self.left_state = False
        return False

    def get_right_click(self):
        state = pygame.mouse.get_pressed()[2]
        if state and not self.right_state:
            self.right_state = True
            return True
        if not state and self.right_state:
            self.right_state = False
        return False
