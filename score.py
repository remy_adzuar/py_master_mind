class Score:

    def __init__(self) -> None:
        self.scores = self.read_file_score()
    
    def read_file_score(self):
        file = open("scores.txt", "r")
        scores = []
        for line in file:
            trim = line.strip()
            split = line.split()
            scores.append(split[0:2])
        file.close()
        return scores
    
    def get_score(self):
        return self.scores
    
    def get_set_name(self):
        names = []
        for name in self.scores:
            names.append(name[0])
        return names

    def add_score(self, score):
        names = self.get_set_name()
        if len(self.scores)<7:
            if score[0] not in names:
                self.scores.append(score)
            else:
                for elem in self.scores:
                    if elem[0] == score[0]:
                        cpt = int(elem[1])+1
                        elem[1] = str(cpt)
            print(self.scores)
            self.rewrite_score()
            return None
    
    def rewrite_score(self):
        file = open("scores.txt", "w")
        for line in self.scores:
            s = str(line[0]+" "+line[1]+"\n")
            file.write(s)
        file.close()

if __name__ == "__main__":

    s = Score()
    print(s.get_score())