import pygame
from config import Config

class Text:
    """
    class qui facile l'affichage des texte a l'ecran
    """

    def __init__(self, content:str, pos, center=False) -> None:
        self.content = content
        self.pos = pos
        self.center = center
        self.tx_color = (255,255,255)
        self.bg_color = (0,0,0)
        self.font_min = pygame.font.Font("freesansbold.ttf", 12)
        self.font_max = pygame.font.Font("freesansbold.ttf", 24)
    
    def print_screen_text(self, config:Config):
        font = self.font_max
        screen = config.get_screen()
        rendu = font.render(self.content, True, self.tx_color, self.bg_color)

        if self.center:
            rect = rendu.get_rect()
            rect.center = self.pos
            screen.blit(rendu, rect)
        else:
            screen.blit(rendu, self.pos)

    def get_content(self):
        return self.content
    
    def get_pos(self):
        return self.pos

    def get_center(self):
        return self.center
    
    def get_tx_color(self):
        return self.tx_color
    
    def get_bg_color(self):
        return self.bg_color
    
    def get_font_min(self):
        return self.font_min
    
    def get_font_max(self):
        return self.font_max